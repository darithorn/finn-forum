var exports = exports || {};
var fs = require ("fs");
var smc = require ("finn-smc");
var finn = require ("finn-server");

exports.configuration = null;

function loadConfigFile (file) {
	fs.exists (file, function (exists) {
		if (exists) {
			fs.readFile (file, function (err, data) {
				if (err) {
					throw err;
				}
				exports.configuration = JSON.parse (data);
			});
		} else {
			console.error("%s does not exist!", file);
			return;
		}
	});
}

var configFile = "config.json";
loadConfigFile (configFile);

exports.cache = new finn.Cache ({
	dir: "assets/"
});

var router = new finn.Router ({
	ip: 8888,
	cache: exports.cache
});
exports.router = router;

router.route ("/", exports.IndexRequest.handle);

// creating threads
router.route ("/section/([0-9]+)/create/?", exports.CreateThreadRequest.handle);

// thread
router.route ("/threads/([0-9]+)/?", exports.ThreadsRequest.handle);

router.route ("/threads/([0-9]+)/reply/?", exports.ReplyRequest.handle);
// user
router.route ("/user/(\\w+)/?", function (req, res) {
	res.end ();
});

