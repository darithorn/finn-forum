var exports = exports || {};
var mustache = require ("mustache");

var SectionRequest = {
	handle: function (req, res) {
		
	},
	render: function (sections) {
		var promise = new Promise (function (resolve, reject) {
			var renderSections = [];
			sections.forEach (function (val, i, arr) {
			renderSections[i] = val;
			exports.Threads.find ({ parent: sections[i].id },
				function (result) {
					renderSections[i].threads = result;
					if (i + 1 === sections.length) {
						console.log (renderSections[0].threads);
						resolve (mustache.render (exports.cache["assets/templates/section.mst"], {
							sections: renderSections
						}));
					}
				});
			});
		});
		return promise;
	}
};

exports.SectionRequest = SectionRequest;
