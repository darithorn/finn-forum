var exports = exports || {};
var smc = require ("finn-smc");
var mustache = require ("mustache");

var ThreadsRequest = {
	handle: function (req, res) {
		if (req.method === "GET") {
			exports.Threads.find ({ id: Number (req.result[1]) }, function (thread) {
				exports.Posts.find ({ parent: Number (req.result[1]) }, function (posts) {
					res.writeHead (200, { "content-type": "text/html" });
					res.write (exports.IndexRequest.render (ThreadsRequest.render (thread, posts)));
					res.end ();
				});
			});
		}
	},
	render: function (thread, posts) {
		return mustache.render (exports.cache["assets/templates/thread.mst"], {
			thread: thread,
			posts: posts
		});
	}
};

exports.ThreadsRequest = ThreadsRequest;
