var exports = exports || {};
var mustache = require ("mustache");
var qs = require ("querystring");

var CreateThreadRequest = {
	handle: function (req, res) {
		if (req.method === "GET") {
			res.writeHead (200, { "content-type": "text/html" });
			res.write (exports.IndexRequest.render (
				CreateThreadRequest.render (req.result[1])));
			res.end ();
		} else if (req.method === "POST") {
			(function () {
				var thread = exports.Thread.new ({
					parent: Number (req.result[1]),
					author: req.post.author,
					subject: req.post.subject
				});
				var post = exports.Post.new ({
					parent: Number (thread.values.id),
					author: req.post.author,
					posted: thread.values.created,
					contents: req.post.text
				});
				exports.Posts.insertOne (post);
				exports.Threads.insertOne (thread);
				exports.router.redirect ("/", res);
			}) ();
		}
	},
	render: function (id) {
		return mustache.render (
			exports.cache["assets/templates/create_thread.mst"], {
				id: id
			});
	}
};

exports.CreateThreadRequest = CreateThreadRequest;
