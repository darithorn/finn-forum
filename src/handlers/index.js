var exports = exports || {};
var mustache = require ("mustache");

var IndexRequest = {
	handle: function (req, res) {
		if (req.method === "GET") {
			exports.Sections.find (function (result) {
				var promise = SectionRequest.render (result);
				promise.then (function (sectionHTML) {
					res.writeHead (200, { "content-type": "text/html" });
					res.write (IndexRequest.render (sectionHTML));
					res.end ();
				});
			});
		}	
	},
	render: function (output) {
		return mustache.render (exports.cache["assets/templates/index.mst"], {
			output: output
		});
	}
};

exports.IndexRequest = IndexRequest;
