var exports = exports || {};

exports.printHelp = function () {
	console.log ("Usage: finn_forum [options]");
	console.log ("Options: ");
	console.log ("--help\t\tDisplay this information");
	console.log ("--config=[file]\t\tSpecify a custom configuration file");
}

exports.commandLineFunctions = {
	"config": function (args) {
		return args[0];
	}
};

exports.handleCommandLineArgs = function () {
	var longArgExp = RegExp ("--(\\w+)=(.+)");
	var shortArgExp = RegExp ("-(\\w+)");
	
	var configFile = "config.json";
	process.argv.forEach (function (val, i, arr) {
		var results = null;
		results = longArgExp.exec (val);
		if (results) {
			
		} else {
			results = shortArgExp.exec (val);
			if (results) {
				
			} else if (val !== "node") {
				console.error ("Unknown argument %s", val);
				printHelp ();
			}
		}
	});
};
