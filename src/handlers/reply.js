var exports = exports || {};
var smc = require ("finn-smc");
var mustache = require ("mustache");

var ReplyRequest = {
	handle: function (req, res) {
		if (req.method === "GET") {
			res.writeHead (200, { "content-type": "text/html" });
			res.write (exports.IndexRequest.render (ReplyRequest.render (req.result[1])));
			res.end ();
		} else if (req.method === "POST") {
			var post = exports.Post.new ({
				parent: Number (req.result[1]),
				author: req.post.author,
				posted: Date.now (),
				contents: req.post.text
			});
			exports.Posts.insertOne (post);
			exports.router.redirect ("/threads/" + req.result[1], res);
		}
	},
	render: function (thread_id) {
		return mustache.render (exports.cache["assets/templates/reply.mst"], {
			id: thread_id
		});
	}
};

exports.ReplyRequest = ReplyRequest;
