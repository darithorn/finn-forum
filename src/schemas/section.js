var exports = exports || {};
var smc = require ("finn-smc");

var Section = new smc.Schema ({
	types: {
		name: String
	}
});

exports.Section = Section;
