var exports = exports || {};
var smc = require ("finn-smc");

var User = new smc.Schema ({
	types: {
		name: String,
		password: Number,
		// t  - create threads
		// p  - post on threads
		// Dt - delete threads
		// Dp - delete posts
		// Ap - edit user permissions
		permissions: String
	},

	getPermissions: function (user) {
		var exp = RegExp ("(t|p|Dt|Dp|Ap)*");
		var results = exp.exec (user.values.permissions);
		return results;
	}
});

exports.User = User;
