var exports = exports || {};
var smc = require ("finn-smc");

var Post = new smc.Schema ({
	types: {
		parent: Number,
		author: String,
		posted: Date,
		contents: String
	}
});

exports.Post = Post;
