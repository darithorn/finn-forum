var exports = exports || {};
var smc = require ("finn-smc");

var Thread = new smc.Schema ({
	types: {
		parent: ObjectId,
		author: String,
		subject: String,
		sticky: Boolean,
		locked: Boolean
	}
});

exports.Thread = Thread;
