var exports = exports || {};
var smc = require ("finn-smc");

var Threads = new smc.Collection ({
	name: "threads",
	type: exports.Thread,
	info: exports.databaseConnection
});

exports.Threads = Threads;
