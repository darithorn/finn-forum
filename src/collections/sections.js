var exports = exports || {};
var smc = require ("finn-smc");

var Sections = new smc.Collection ({
	name: "sections",
	type: exports.Section,
	info: exports.databaseConnection
});

exports.Sections = Sections;
