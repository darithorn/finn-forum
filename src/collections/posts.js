var exports = exports || {};
var smc = require ("finn-smc");

var Posts = new smc.Collection ({
	name: "posts",
	type: exports.Post,
	info: exports.databaseConnection
});

exports.Posts = Posts;
