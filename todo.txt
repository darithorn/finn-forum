Load configuration settings from a file:
 	 title 		   		<required>
	 ip					[default "127.0.0.1"]
	 port				[default "25000"]
	 theme				[default "basic/"]
	 template_manager	[default "mustache"]
	 database_url	   	[default "mongodb://localhost/"]
	 
Create routes for:
	   sections     /section/1/
	   threads		/section/1/t/1
	   posts		/section/1/t/1/p/1
	   users		/user/darithorn
