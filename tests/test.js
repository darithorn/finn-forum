var exp = new RegExp ("^.*\\.\\w*$");

var tests = [
	"/assets/section/0/create/styles/base.css",
	"/assets/sec.tion/0/30.0/derp.js",
	"finn.derp.min.js",
	"derp.min/not",
	"nothing"
];

for (var i in tests) {
	var results = exp.exec (tests[i]);
	if (results) {
		console.log ("`%s` passed!", tests[i]);
	} else {
		console.log ("`%s` failed!", tests[i]);
	}
}

var url_tests = [
	"/assets/section/0/create/styles/base.css"
];

var path = "/assets/section/0/create";
var assets = "./assets/";

for (var i in url_tests) {
	var t = url_tests[i];
	console.log (assets + t.replace (path, ""));
}
