module.exports = function (grunt) {
	var sourceFiles = [
		"src/connection.js",
		"src/schemas/post.js",
		"src/schemas/thread.js",
		"src/schemas/user.js",
		"src/schemas/section.js",
		"src/collections/sections.js",
		"src/collections/threads.js",
		"src/collections/posts.js",
		"src/handlers/commandLineArgs.js",
		"src/handlers/user.js",
		"src/handlers/section.js",
		"src/handlers/createthread.js",
		"src/handlers/reply.js",
		"src/handlers/threads.js",
		"src/handlers/index.js",
		"src/utility.js",
		"src/server.js"
	];
	grunt.initConfig ({
		pkg: grunt.file.readJSON ("package.json"),
		uglify: {
			finn_forum: {
				files: {
					"dist/finn.forum.min.js": sourceFiles,
					"tests/finn.forum.min.js": sourceFiles
				}
			}
		}
	});

	grunt.loadNpmTasks ("grunt-contrib-uglify");
};
